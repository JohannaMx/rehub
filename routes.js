const sqlite3 = require("sqlite3").verbose();
const db = new sqlite3.Database(__dirname + '/datenbank.db');
const bcrypt = require("bcrypt");
const router = require('express').Router();

// to signup
router.get('/', function(req, res) {
    res.redirect('/landing');
});
// statistic
router.get('/statistic', function(req, res) {

    if (req.session.userID != undefined) {
      db.all(`SELECT date, ROUND(SUM(s.caffQuantity*c.content*0.01),2) AS total, SUM(s.alcQuantity*a.content) AS alctotal
      FROM statistics AS s, caffeine AS c, alcohol AS a
      WHERE userID = ${req.session.userID}
      AND s.caffID = c.caffID 
      AND s.alcID = a.alcID
      GROUP BY date
      `, function(err,rows){
          res.render('statistic', 
          {
              totals: rows,
              userWeight: req.session.weight,
          });
        });
        
     } 
     else{
        res.redirect('/login');
    }
    });

// caffeine
router.get('/caffeine', function(req, res) {
    if (req.session.mail != undefined) {
        db.all(`SELECT * FROM caffeine`, function(err, rows){
                res.render('caffeine', {"caffeine_names" : rows});
        });
        
    }
    else {
        res.redirect('/');
    }
});

// alcohol
router.get('/alcohol', function(req, res) {
    if (req.session.mail != undefined) {
        db.all(`SELECT * FROM alcohol`, function(err, rows){
                res.render('alcohol', {"alcohol_names" : rows});
        });       
    }
    else {
        res.redirect('/');
    }
});

// nicotin
router.get('/nicotin', function(req, res) {
    if (req.session.userID != undefined){
        db.all(`SELECT date, SUM(zigAmount) AS zig
        FROM statistics
        WHERE userID =${req.session.userID}
        GROUP BY date`, function(err, rows){
            res.render('nicotin', {"zigCount": rows});
        });
    }
    else {
        res.redirect('login');
    }
    
});
// profile
router.get('/profile', function(req, res) {
   
    if(req.session.mail == undefined) {
        res.redirect('/');
    }
    else {
        db.all(`SELECT weight, height, age, gender, userID FROM users WHERE email='${req.session.mail}'`,
        function(err, rows){ 
            
            let user_w = rows[0].weight;
            let user_h = rows[0].height;
            let user_a = rows[0].age;
            let user_g = rows[0].gender;

            req.session.userID = rows[0].userID;
            req.session.weight = rows[0].weight;
            req.session.age = rows[0].age;

            if (rows[0].weight != null ) {
                 user_w = rows[0].weight;
                }
            else {
                user_w = 0;
            }

            if (rows[0].height != null ) {
                user_h = rows[0].height;
               }
            else {
               user_h = 0;
            }

           if (rows[0].height != null ) {
                user_a = rows[0].age;
            }
           else {
                user_a = 0;
            }

            res.render('profile', {
            'user': req.session.mail, 
            'user_w_o': user_w,
            'user_h_o': user_h,
            'user_a_o': user_a,
            'user_g_o': user_g
         }); 

        });        
    }
});

//User-entry Formular auswerten
router.post('/user-data-entry', function(req, res) {
    let param_user_weight = req.body.user_weight;
    let param_user_height = req.body.user_height;
    let param_user_age = req.body.user_age;
    let param_user_gender = req.body.user_gender;
    req.session.weight = req.body.user_weight;
    db.run(
        `UPDATE users
         SET weight = ${param_user_weight},
         height = ${param_user_height},
         age = ${param_user_age},
         gender = '${param_user_gender}'
         WHERE email = '${req.session.mail}' 
         `, 
    function(err){
        res.redirect('/profile');
    });
});

//Koffein Produkt hinzufügen
router.post('/addCaffe',function(req, res){
    let param_newCaffname = req.body.newCaff_name;
    let param_newCaffcontent = req.body.newCaff_content;

    db.run(
        `INSERT INTO caffeine(name, content) 
        VALUES ('${param_newCaffname}', ${param_newCaffcontent})`,
        function (err){
            res.redirect('/caffeine');
        }
    );
});

//Alkohol Produkt hinzufügen
router.post('/addAlc',function(req, res){
    let param_newAlcname = req.body.newAlc_name;
    let param_newAlccontent = (req.body.newAlc_content/100);

    db.run(
        `INSERT INTO alcohol(name, content) 
        VALUES ('${param_newAlcname}', ${param_newAlccontent})`,
        function (err){
            res.redirect('/alcohol');
        }
    );
});


/**
 * SIGNUP
 */

// signup template renderer
router.get('/signup', function(req, res) {
    res.render('signup', {
        error: req.query.error
    });
});

// signup post handler
router.post('/signup', function(req, res) {
    const param_email = req.body.mail;
    const param_password = req.body.password;
    const param_password_confirm = req.body.password_confirm;
    req.session.mail = param_email;

    if(param_password == param_password_confirm && param_password != "") {
    //Existiert die Email bereits?
        db.all(
            `SELECT userID FROM users WHERE email='${param_email}'`,
            function(err, rows) {
                if (rows && rows.length > 0) {
                    res.redirect('/signup?error=1');
                    //Account existiert bereits
                } else {
                    // passwort --> hash
                    const hash = bcrypt.hashSync(param_password, 12);
                    if(param_email != "") {
                    db.run(
                        `INSERT INTO users(email, hash) VALUES('${param_email}', '${hash}')`,
                        function(err){
                            res.redirect('/profile'); 
                        }
                    ); 
                    }
                    else {
                        res.redirect('/signup?error=3');
                    }
                }
            }
        );
    } else if(param_password == "") {
        // passwortbestätigung falsch
        res.redirect('/signup?error=3');
    }
    else{
        res.redirect('/signup?error=2');
    }
});
/**
 * LOGIN
 */
//Ort der Funktion
router.get('/login', function(req, res) {
    res.render('login',{
        error: req.query.error
    });
});
//Aufruf der Funktion + Variablen
 router.post('/login-req', function(req, res){
    const param_email = req.body.mail;
    const param_password = req.body.password;
    req.session.mail = param_email;

//Überprüfung des Passworts
    if(param_email != ""){
        db.all(`SELECT hash FROM users WHERE email='${param_email}'`,
        function(err, rows){
            
            if (rows.length != 0){
                const hash = rows[0].hash;
                const isValid = bcrypt.compareSync(param_password, hash);

                if(isValid == true){
                    //res.render("profile", {'user': param_email});
                    res.redirect('profile');
                }
                else{
                    res.redirect('/login?error=1'); 
                    //Passwort falsch
                }
            }
            else{
                    res.redirect('/login?error=2'); 
                    //Account existiert nicht
            }
        });
    }
    else{
        res.redirect('/login?error=3'); 
        //Leere Felder
    }
 });

/**
 * LANDING
 */
router.get('/landing', function(req, res) {
     res.render('landing');
});

// logout
router.get('/logout', function(req, res, next) {
    if(req.session) {
        req.session.destroy(function(err){
            if(err) {
                return next(err);
            } 
            else {
                return res.redirect('/');
            }
        });
    }
});



/**
 * consumeLog
 */

/* Koffein */

router.post('/caffeine_entry', function(req, res){
    
    let valueVar = req.body.caff_butt;
    let param_caffID = (valueVar % 100); 
    let param_caffQuantity = (valueVar - param_caffID)/100;
    let date = new Date();
    let param_currentDate = Math.round(date.getTime()/86400000 - 0.5);
    let param_customCaffID = req.body.caff_sub;
    let param_customMl = req.body.custom_ml[(param_customCaffID-1)];   

    db.run(
        `INSERT INTO statistics (userID, caffQuantity,  date, caffID, alcID, alcQuantity , zigAmount)
        VALUES(${req.session.userID},${param_caffQuantity}, '${param_currentDate}', ${param_caffID}, 1, 0, 0)`,
        function(err){
            res.redirect('/caffeine');
        
        });
    if(param_customMl != null) {   
    db.run(
        `INSERT INTO statistics (userID, caffQuantity, date, caffID, alcID, alcQuantity, zigAmount)
        VALUES(${req.session.userID}, ${param_customMl} , '${param_currentDate}', ${param_customCaffID}, 1, 0, 0)`,
        function(err){
             console.log(err);
        });
    } 
});



/* Alkohol */

router.post('/alcohol_entry', function(req, res){
    
    let valueVar_alc = req.body.alc_butt;
    let param_alcID = (valueVar_alc % 100); 
    let param_alcQuantity = (valueVar_alc - param_alcID)/100;
    let date = new Date();
    let param_currentDate = Math.round(date.getTime()/86400000);
    let param_customAlcID = req.body.alc_sub;
    let param_customMl_alc = req.body.custom_ml_alc[(param_customAlcID-1)];   
    db.run(
        `INSERT INTO statistics (userID, alcQuantity,  date, alcID, caffID, caffQuantity, zigAmount)
        VALUES(${req.session.userID},${param_alcQuantity}, '${param_currentDate}', ${param_alcID}, 1, 0, 0)`,
        function(err){
            res.redirect('/alcohol');
        
        });
    if(param_customMl_alc != null) {   
    db.run(
        `INSERT INTO statistics (userID, alcQuantity, date, alcID, caffID, caffQuantity , zigAmount)
        VALUES(${req.session.userID}, ${param_customMl_alc} , '${param_currentDate}', ${param_customAlcID}, 1, 0, 0)`,
        function(err){
             console.log(err);
        });
    } 
});

/* Nikotin */

router.post('/nic_entry', function(req, res){
    let zigCount = req.body.nic_button;
    let date = new Date();
    let param_currentDate = Math.round(date.getTime()/86400000);
    db.run(
        `INSERT INTO statistics (userID, alcQuantity,  date, alcID, caffID, caffQuantity, zigAmount)
        VALUES(${req.session.userID}, 0 , '${param_currentDate}', 1 , 1 , 0 , ${zigCount})`,
        function(err){ 
            setTimeout(function(){ 
                res.redirect('/nicotin');
            },1200);
        });
});
module.exports = router;
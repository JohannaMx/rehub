const express = require("express");
const app = express();
const bodyParser = require("body-parser");
const session = require('express-session');
app.use(bodyParser.urlencoded({extended: true}));

app.use(session({
    name: "rehub-sess",
    cookie: {},
    secret: 'pups'

}));

app.engine("ejs", require("ejs").__express);
app.set("view engine", "ejs");
app.set('views', __dirname + '/views')


app.use('/static', express.static('static'));

app.use('/', require('./routes'));


app.listen(3000, function(){
    console.log("Läuft auf 3000");
});



## REHUB - control your consumption
Auf der interaktiven Website von REHUB kann eine eigenes Profil erstellt werden. Anschließend kann täglich der Koffein, Alkohol und die Anzahl der Zigaretten hinzugefügt werden. Die eingetragenen Werte können anschließend in einem Wochen- bzw. Monatsrückblick in Form eines Diagramms eingesehen werden.

Ein Projekt von Jonas Christmann, Karl Marggraff, André Schuldt und Johanna Meyer. Enstanden ist dieses Projekt im Rahmen des Kurses Angewandte Programmierung als Projekt A.


## Startseite

![Landing Page](images/landingPage.png)

## Registrieren

![signup Page](images/signUp.png)

## Login

![signup Page](images/login.png)

## Profil

![profile Page](images/profile.png)

## Koffein hinzufügen

![caffeine Page](images/caffeine.png)

## Alkohol hinzufügen

![alcohol Page](images/alcohol.png)

## Statistik: Wochen- und Monatsüberblick

![statistic Page](images/statistic.png)

## Zigarette hinzufügen

![nicotine Page](images/nicotine.png)